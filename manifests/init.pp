class qa-apache {

  package { 'apache2':
    ensure => present,
  }

  file { '/var/www':
    ensure => directory,
  }

  file { '/var/www/html':
    ensure => directory,
  }

  file { '/var/www/html/index.html':
    ensure => file,
    content => template('qa-apache/index.html.erb'),
  }

  service { 'apache2':
    ensure => running,
  }

  file { '/etc/apache2/apache2.conf':
    ensure => file,
    owner => 'root',
    group => 'root',
    source => 'puppet:///modules/qa-apache/apache2.conf',
    require => Package['apache2'],
  }

}







